The JS rendering layer built on top of the ShadowCMS. It provides tools for easy
 creation of forms, rendering the content editing interface & more

Example:
 ![alt text](screenshot.png)
