<?php
if(isset($bUsePolyFill) && $bUsePolyFill)
{
	?>
    <script src="/<?=$GLOBALS["relative_path"]?>frontEnd/ShadowJS/libraries/polyfill.js?v=<?=VERSION?>"></script>

	<?php
}?>

<script src="/<?=$GLOBALS["relative_path"]?>frontEnd/ShadowJS/libraries/jquery-3.1.0.min.js?v=<?=VERSION?>"></script>

<link href="/<?=$GLOBALS["relative_path"]?>frontEnd/ShadowJS/libraries/bootstrap.min.css?v=<?=VERSION?>" rel="stylesheet">

<script src="/<?=$GLOBALS["relative_path"]?>frontEnd/ShadowJS/libraries/bootstrap.min.js?v=<?=VERSION?>"></script>

<link rel="stylesheet" type="text/css" href="/<?=$GLOBALS["relative_path"]?>frontEnd/ShadowJS/libraries/datatables.min.css?v=<?=VERSION?>"/>
<script type="text/javascript" src="/<?=$GLOBALS["relative_path"]?>frontEnd/ShadowJS/libraries/pdfmake.min.js?v=<?=VERSION?>"></script>
<script type="text/javascript" src="/<?=$GLOBALS["relative_path"]?>frontEnd/ShadowJS/libraries/vfs_fonts.js?v=<?=VERSION?>"></script>
<script type="text/javascript" src="/<?=$GLOBALS["relative_path"]?>frontEnd/ShadowJS/libraries/datatables.min.js?v=<?=VERSION?>"></script>

<link href="/<?=$GLOBALS["relative_path"]?>frontEnd/ShadowJS/libraries/select2.min.css?v=<?=VERSION?>" rel="stylesheet" />
<script src="/<?=$GLOBALS["relative_path"]?>frontEnd/ShadowJS/libraries/select2.min.js?v=<?=VERSION?>"></script>

<script src="/<?=$GLOBALS["relative_path"]?>frontEnd/ShadowJS/libraries/modernizr.min.js?v=<?=VERSION?>"></script>

<script src="/<?=$GLOBALS["relative_path"]?>frontEnd/ShadowJS/libraries/custom.js?v=<?=VERSION?>"></script>

<script src="/<?=$GLOBALS["relative_path"]?>frontEnd/ShadowJS/libraries/jquery.jsonrpc.js?v=<?=VERSION?>"></script>

<script src="/<?=$GLOBALS["relative_path"]?>frontEnd/ShadowJS/Renderer/Utils.js?v=<?=VERSION?>"></script>
<script src="/<?=$GLOBALS["relative_path"]?>frontEnd/ShadowJS/Renderer/Renderer.js?v=<?=VERSION?>"></script>
<script src="/<?=$GLOBALS["relative_path"]?>frontEnd/ShadowJS/Renderer/Elements/BaseElement.js?v=<?=VERSION?>"></script>

<script src="/<?=$GLOBALS["relative_path"]?>frontEnd/ShadowJS/Renderer/Elements/TreeView.js?v=<?=VERSION?>"></script>
<script src="/<?=$GLOBALS["relative_path"]?>frontEnd/ShadowJS/Renderer/Elements/Tabs.js?v=<?=VERSION?>"></script>

<script src="/<?=$GLOBALS["relative_path"]?>frontEnd/ShadowJS/Renderer/Elements/Table.js?v=<?=VERSION?>"></script>

<script src="/<?=$GLOBALS["relative_path"]?>frontEnd/ShadowJS/Renderer/Elements/TablePlugins/BaseTablePlugin.js?v=<?=VERSION?>"></script>
<script src="/<?=$GLOBALS["relative_path"]?>frontEnd/ShadowJS/Renderer/Elements/TablePlugins/IndividualColumnsSearchPlugin.js?v=<?=VERSION?>"></script>

<script src="/<?=$GLOBALS["relative_path"]?>frontEnd/ShadowJS/Renderer/Elements/Reorder.js?v=<?=VERSION?>"></script>

<script src="/<?=$GLOBALS["relative_path"]?>frontEnd/ShadowJS/Renderer/Elements/Form.js?v=<?=VERSION?>"></script>
<script src="/<?=$GLOBALS["relative_path"]?>frontEnd/ShadowJS/Renderer/Elements/Form/BaseFormElement.js?v=<?=VERSION?>"></script>
<script src="/<?=$GLOBALS["relative_path"]?>frontEnd/ShadowJS/Renderer/Elements/Form/ColorRow.js?v=<?=VERSION?>"></script>
<script src="/<?=$GLOBALS["relative_path"]?>frontEnd/ShadowJS/Renderer/Elements/Form/BootstrapWYSIWYG.js?v=<?=VERSION?>"></script>
<script src="/<?=$GLOBALS["relative_path"]?>frontEnd/ShadowJS/Renderer/Elements/Form/MultiImage.js?v=<?=VERSION?>"></script>
<script src="/<?=$GLOBALS["relative_path"]?>frontEnd/ShadowJS/Renderer/Elements/Form/Multiselect.js?v=<?=VERSION?>"></script>
<script src="/<?=$GLOBALS["relative_path"]?>frontEnd/ShadowJS/Renderer/Elements/Form/Image.js?v=<?=VERSION?>"></script>
<script src="/<?=$GLOBALS["relative_path"]?>frontEnd/ShadowJS/Renderer/Elements/Form/File.js?v=<?=VERSION?>"></script>

<script src="/<?=$GLOBALS["relative_path"]?>frontEnd/ShadowJS/Renderer/Elements/FormPlugins/BaseFormPlugin.js?v=<?=VERSION?>"></script>
<script src="/<?=$GLOBALS["relative_path"]?>frontEnd/ShadowJS/Renderer/Elements/FormPlugins/EditNutrientsPlugin.js?v=<?=VERSION?>"></script>
<script src="/<?=$GLOBALS["relative_path"]?>frontEnd/ShadowJS/Renderer/Elements/FormPlugins/PhoneNumberPlugin.js?v=<?=VERSION?>"></script>

<script src="/<?=$GLOBALS["relative_path"]?>frontEnd/ShadowJS/libraries/jQuery.extendext.min.js?v=<?=VERSION?>"></script>

<script src="/<?=$GLOBALS["relative_path"]?>frontEnd/ShadowJS/libraries/datetime-local-polyfill.min.js?v=<?=VERSION?>"></script>

<script src="/<?=$GLOBALS["relative_path"]?>frontEnd/ShadowJS/libraries/sortable.min.js?v=<?=VERSION?>"></script>

<script src="/<?=$GLOBALS["relative_path"]?>frontEnd/ShadowJS/libraries/cropper.min.js?v=<?=VERSION?>"></script>
<link href="/<?=$GLOBALS["relative_path"]?>frontEnd/ShadowJS/libraries/cropper.min.css?v=<?=VERSION?>" rel="stylesheet">

<script src="/<?=$GLOBALS["relative_path"]?>frontEnd/ShadowJS/libraries/autosize.min.js?v=<?=VERSION?>"></script>

<script src="/<?=$GLOBALS["relative_path"]?>frontEnd/ShadowJS/libraries/bootstrap-wysiwyg.js?v=<?=VERSION?>"></script>
<script src="/<?=$GLOBALS["relative_path"]?>frontEnd/ShadowJS/libraries/jquery.hotkeys.js?v=<?=VERSION?>"></script>
<script src="/<?=$GLOBALS["relative_path"]?>frontEnd/ShadowJS/libraries/jquery.mb.browser.min.js?v=<?=VERSION?>"></script>
<script src="/<?=$GLOBALS["relative_path"]?>frontEnd/ShadowJS/libraries/jquery.webkitresize.nonifrm.js?v=<?=VERSION?>"></script>
<script src="/<?=$GLOBALS["relative_path"]?>frontEnd/ShadowJS/libraries/jquery.wysiwyg-resize.nonifrm.js?v=<?=VERSION?>"></script>

<link href="/<?=$GLOBALS["relative_path"]?>frontEnd/ShadowJS/libraries/style.css?v=<?=VERSION?>" rel="stylesheet">

<link href="/<?=$GLOBALS["relative_path"]?>frontEnd/ShadowJS/libraries/font-awesome.css" rel="stylesheet">

<link rel="stylesheet" href="/<?=$GLOBALS["relative_path"]?>frontEnd/ShadowJS/libraries/jquery-ui.css">
<script src="/<?=$GLOBALS["relative_path"]?>frontEnd/ShadowJS/libraries/jquery-ui.min.js"></script>

<script src="/<?=$GLOBALS["relative_path"]?>frontEnd/ShadowJS/libraries/jquery.nestable.js?v=<?=VERSION?>"></script>


<script src="/<?=$GLOBALS["relative_path"]?>frontEnd/ShadowJS/libraries/bootstrap-datepicker.min.js?v=<?=VERSION?>"></script>

<link href="/<?=$GLOBALS["relative_path"]?>frontEnd/ShadowJS/libraries/bootstrap-datepicker3.min.css?v=<?=VERSION?>" rel="stylesheet">


<script>
    $.jsonRPC.setup({
        endPoint: '/<?=$GLOBALS["relative_path"]?>' + "privateEndpoint"
    });
</script>