function arrayCombine(arrKeys, arrValues)
{
	let arrResult = {};
	for (let i = 0; i < arrKeys.length; i++)
		arrResult[arrKeys[i]] = arrValues[i];
	return arrResult;
}

function isString(arg)
{
	return typeof arg === 'string';
}

function createElement(strElementName, arrElementProps, mxElementContent, arrElementEvents)
{
	arrElementProps = (typeof arrElementProps === "object") ? arrElementProps : {};
	arrElementEvents = (typeof arrElementEvents === "object") ? arrElementEvents : {};

	let elNew = document.createElement(strElementName);
	for(let i in arrElementProps)
	{
		elNew.setAttribute(i, arrElementProps[i]);
	}
	if(mxElementContent !== undefined)
	{
		if(isString(mxElementContent))
			elNew.appendChild(document.createTextNode(mxElementContent));
		else if(Array.isArray(mxElementContent))
			for(let i in mxElementContent)
			{
				elNew.appendChild(mxElementContent[i]);
			}
		else
			elNew.appendChild(mxElementContent);
	}

	if(arrElementEvents !== undefined)
	{
		for(let strEventName in arrElementEvents)
		{
			elNew.addEventListener(strEventName, arrElementEvents[strEventName]);
		}
	}

	return elNew;
}

function createText(strText)
{
	return document.createTextNode(strText);
}

function array_key_exists(strKey, arrObject)
{
	return (arrObject[strKey] !== undefined);
}

function array()
{
	let arr = [];
	for(let i in arguments)
		arr.push(arguments[i]);
	return arr;
}

function capitalizeFirstLetter(string) {
	return string[0].toUpperCase() + string.slice(1);
}

(function($) {
	$.fn.goTo = function() {
		$('html, body').animate({
			scrollTop: $(this).offset().top + 'px'
		}, 'fast');
		return this; // for chaining...
	}
})(jQuery);