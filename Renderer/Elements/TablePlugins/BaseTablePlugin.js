class BaseTablePlugin{
	/**
	 * @param {Table} table
	 * @param {Object} arrOptions
	 */
	constructor(table, arrOptions)
	{
		this.table = table;
		this.arrOptions = arrOptions;
	}

	initialize()
	{
		console.error("Table Plugin Initialize not defined");
	}
}