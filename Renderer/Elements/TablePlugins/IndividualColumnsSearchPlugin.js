class IndividualColumnsSearchPlugin extends BaseTablePlugin{

	initialize()
	{
		const $elSearchInput = $('.dataTables_filter input');

		const elFilterSelect = createElement("select", {class: "form-control input-sm"}, "", {change: (e) => {
			fnSearch($elSearchInput.val());
		}});

		elFilterSelect.appendChild(createElement("option", {value: "all"}, "All"));
		for (const i in this.table.arrColumns)
		{
			if(this.table.arrColumns[i]["name"])
				elFilterSelect.appendChild(createElement("option", {value: this.table.arrColumns[i]["name"]}, this.table.arrColumns[i]["title"]));
		}
		$(elFilterSelect).insertBefore('.dataTables_filter input');

		this.table.dataTable.search('')
			.columns().search('').draw();

		const fnSearch = (strSearch) => {
			if (elFilterSelect.value === "all")
			{
				this.table.dataTable
					.columns().search('');
				this.table.dataTable
					.search(strSearch)
					.draw();
			}
			else
			{
				this.table.dataTable.search(strSearch)
					.columns().search('')
					.columns(elFilterSelect.value + ":name").search(strSearch).draw();
			}
		};

		setTimeout(() => {
            $elSearchInput.unbind().keyup((e, settings) => {
                fnSearch(e.target.value);
            });
        }, 100);

	}
}