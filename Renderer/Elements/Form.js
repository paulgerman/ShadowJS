class Form extends BaseElement{
	constructor(elParent, arrData, arrSelectValues, arrHiddenKeysValues, bAllowAdd)
	{
		super(elParent, arrData, arrSelectValues);
		this.arrHiddenKeysValues = (typeof arrHiddenKeysValues === "object")? arrHiddenKeysValues : {};
		this.bAllowAdd = (bAllowAdd === undefined) ? true : bAllowAdd;
		this.arrFormInputs = {};
		this.arrFormCustomElements = {};
		this.bEditMode = false;
		this.arrPromisesSubmit = [];
		this.arrPlugins = [];

		this.arrValidationFns = [];
	}

	render()
	{
		let elForm = document.createElement("form");
		elForm.className = "form-horizontal";
		elForm.addEventListener("submit", (e) => {
			e.preventDefault();
			this.submit();
			return false;
		});

		if(!this.bAllowAdd)
			elForm.style.display = "none";
		this.elForm = elForm;

		this.elFormContent = createElement("div");
		this.elFormButtons = createElement("div");

		for(let strKey in this.arrHiddenKeysValues)
		{
			let elInput = document.createElement("input");
			elInput.type = "hidden";
			elInput.value = this.arrHiddenKeysValues[strKey];
			this.elFormContent.appendChild(elInput);
		}

		for(let strKey in this.arrData["cols"])
		{
			if(
				this.arrHiddenKeysValues[strKey] === undefined
				&& this.arrData["readOnlyKeys"].indexOf(strKey) === -1
			)
			this.addItem(this.arrData["cols"][strKey], strKey, this.elFormContent);
		}

		const elButtonAdd = document.createElement("button");
		elButtonAdd.type = "submit";
		elButtonAdd.className = "btn btn-default";
		elButtonAdd.appendChild(createElement("span", {class:"glyphicon glyphicon-refresh glyphicon-refresh-animate hidden"}));
		elButtonAdd.appendChild(document.createTextNode("Add"));
		elButtonAdd.style.marginRight = "10px";

		this.elButtonAdd = elButtonAdd;

		const elButtonSave = document.createElement("button");
		elButtonSave.type = "submit";
		elButtonSave.className = "btn btn-default";
		elButtonSave.appendChild(createElement("span", {class:"glyphicon glyphicon-refresh glyphicon-refresh-animate hidden"}));
		elButtonSave.appendChild(document.createTextNode("Save"));
		elButtonSave.style.marginRight = "10px";


		elButtonSave.style.display = "none";
		this.elButtonSave = elButtonSave;

		const elButtonClose = document.createElement("button");
		elButtonClose.type = "button";
		elButtonClose.className = "btn btn-default";
		elButtonClose.appendChild(document.createTextNode("Reset"));
		elButtonClose.addEventListener("click", () => {
			if(this.fnCloseEditCallback)
				this.fnCloseEditCallback();
			else
			{
				if(!this.bEditMode)
				{
					const bContinue = confirm("Are you sure you want to reset the form?");
					if(!bContinue)
						return;
				}
				this.resetState();
			}
		});

		this.elButtonClose = elButtonClose;

		this.elFormButtons.appendChild(elButtonAdd);
		this.elFormButtons.appendChild(elButtonSave);
		this.elFormButtons.appendChild(elButtonClose);

		if(this.elKeyDiv)
			this.elKeyDiv.style.display = "none";

		this.elForm.appendChild(this.elFormContent);
		this.elForm.appendChild(this.elFormButtons);
		this.elParent.appendChild(elForm);

		for(const i in this.arrPlugins)
			this.arrPlugins[i].initialize();

		//reset after initializing plugins because they have reset method too
		this.resetState();
	}

	toggleLoading()
	{
		const elButton = this.bEditMode?this.elButtonSave:this.elButtonAdd;

		if(!elButton.disabled)
		{
			elButton.disabled = true;
            elButton.firstElementChild.classList.remove("hidden");
        }
        else
		{
            elButton.disabled = false;
            elButton.firstElementChild.classList.add("hidden");
        }
	}

	submit()
	{
        this.toggleLoading();
		const arrStateData = this.getState(true).arrData;

		if(this.arrValidationFns.length > 0)
			for(let i in this.arrValidationFns)
				if(!this.arrValidationFns[i](arrStateData))
				{
                    this.toggleLoading();
                    return false;
                }



		Promise.all(this.arrPromisesSubmit).then((args) => {
			for(let i in args)
				if(args[i] !== undefined)
					arrStateData[args[i]["key"]] = args[i]["value"];

			const arrFilteredData = this.filterData(arrStateData);

			if(this.bEditMode)
				this.fnEditCallback(arrFilteredData[this.arrData["indexKey"]], arrFilteredData);
			else
				this.fnAddCallback(arrFilteredData);

			this.arrPromisesSubmit = [];

			for(let i in this.arrPlugins)
				this.arrPlugins[i].afterSubmit(arrFilteredData);

		});
	}

	removeItem()
	{

	}

	addItem(arrItem, strKey, elParent)
	{
		if(isString(arrItem))
		{
			$(elParent).append(arrItem);
			return;
		}

		const elDiv = document.createElement("div");
		elDiv.className = "form-group";
		let strDisplayName = strKey;
		if(arrItem["displayName"] !== undefined)
			strDisplayName = arrItem["displayName"];

		const elLabelDiv = createElement("div", {class: "col-sm-2"});
		const elLabel = createElement("label", {class:"control-label", style:"width:100%"});
		if(arrItem["internalAttributes"] && arrItem["internalAttributes"]["required"] === true)
			elLabel.appendChild(document.createTextNode(strDisplayName+"*"));
		else
			elLabel.appendChild(document.createTextNode(strDisplayName));
		elLabelDiv.appendChild(elLabel);
		elDiv.appendChild(elLabelDiv);


		if(arrItem["tooltip"] !== undefined)
		{
			elLabelDiv.appendChild(createElement("br"));
			elLabelDiv.appendChild(createElement("span", {class: "form-tooltip"}, " " + arrItem["tooltip"]));
		}

		const elInputDiv = createElement("div", {class: "col-sm-10"});
		elDiv.appendChild(elInputDiv);
		if(arrItem["internalType"] !== undefined)
		{
			arrItem["internalType"] = capitalizeFirstLetter(arrItem["internalType"]);

			if(BaseElement.classes[arrItem["internalType"]] !== undefined)
			{
				let baseElement = new BaseElement.classes[arrItem["internalType"]](elInputDiv, [], arrItem, this, strKey);
				baseElement.renderEdit();
				this.arrFormCustomElements[strKey] = baseElement;
			}
			else
			{
				elInputDiv.appendChild(document.createTextNode("SAMPLE FOR " + arrItem["internalType"]));
				//throw ("undefined class" + arrOptions["internalType"]);
			}
		}
		else
		{
			this.arrFormInputs[strKey] = this.defaultElementEdit(strKey, elInputDiv);
		}

		if(strKey === this.arrData["indexKey"])
			this.elKeyDiv = elDiv;

		elParent.appendChild(elDiv);
	}


	defaultElementEdit(strKey, elParent)
	{
		let arrCol = this.arrData["cols"][strKey];
		let arrSelectOptions = [];
		let arrEnumValues = undefined;
		if(this.arrData["enumKeys"] !== undefined)
			arrEnumValues = this.arrData["enumKeys"][strKey];

		if(arrCol["type"] === undefined)
		{
			arrCol["type"] = "text";
		}

		if(arrCol["type"] === "select")
		{
			if(arrCol["placeholder"] !== undefined)
				arrSelectOptions.push({
					"text": arrCol["placeholder"],
					"value": "",
				});
			
			if (arrEnumValues !== undefined && this.arrOptions[strKey] === undefined)
			{
				arrCol["type"] = "select";
				for (let i in arrEnumValues)
				{
					let strText;
					if (arrCol["displayOptions"] !== undefined)
						strText = arrCol["displayOptions"][i];
					else
						strText = arrEnumValues[i];

					arrSelectOptions.push({
						"text": strText,
						"value": arrEnumValues[i],
					});
				}
			}
		}

		let arrElementOptions = {};
		let elSelect = undefined;
		let elInput;
		switch(arrCol["type"])
		{
			case "select":
			{
				if (arrSelectOptions.length === 0)
				{
					if (this.arrOptions[strKey] === undefined)
						throw "You haven't defined the select options for key " + strKey;
					arrSelectOptions = this.arrOptions[strKey];
				}

				arrElementOptions = {
					"class": "form-control",
				};

				if (array_key_exists("internalAttributes", arrCol))
				{
					arrElementOptions = $.extend({}, arrCol["internalAttributes"], arrElementOptions);
				}
				elSelect = createElement("select", arrElementOptions);

				this.selectDisplay(arrSelectOptions, elSelect);

				elParent.appendChild(elSelect);
				elInput = elSelect;
				break;
			}
			case "textarea":
			{
				arrElementOptions =	{
					style: "overflow-y: hidden; min-height: 25px; max-height: 300px;",
					class: "form-control"
				};

				if (array_key_exists("internalAttributes", arrCol))
				{
					arrElementOptions = $.extend({}, arrCol["internalAttributes"], arrElementOptions);
				}

				elInput = createElement(
					"textarea",
					arrElementOptions
				);

				elInput.addEventListener('shadowjs:changed', () => {
					autosize.update(elInput);
				}, false);

				elParent.appendChild(elInput);
				autosize(elInput);
				setTimeout(() => {autosize.update(elInput);}, 10);

				break;
			}
			case "multiselect":
			{
				if (arrSelectOptions.length === 0)
				{
					if (this.arrOptions[strKey] === undefined)
						console.error("You haven't defined the select options for key " + strKey);
					arrSelectOptions = this.arrOptions[strKey];
				}

				arrElementOptions = {
					style: "width:100%",
					class: "form-control",
					multiple: "multiple",
				};


				if (array_key_exists("internalAttributes", arrCol))
				{
					arrElementOptions = $.extend({}, arrCol["internalAttributes"], arrElementOptions);
				}

				elSelect = createElement(
					"select",
					arrElementOptions
				);

				this.selectDisplay(arrSelectOptions, elSelect);

				elParent.appendChild(elSelect);

				elInput = elSelect;
				break;
			}
			case "text":
			{
				arrElementOptions = {
					class: "form-control"
				};

				if (array_key_exists("internalAttributes", arrCol))
				{
					arrElementOptions = $.extend({}, arrCol["internalAttributes"], arrElementOptions);
				}

				elInput = createElement(
					"input",
					arrElementOptions
				);
				if (strKey === this.arrData["indexKey"])
					elInput.disabled = true;

				elParent.appendChild(elInput);
				break;
			}
			default:
			{
				console.error("Type "+arrCol["type"]+" is not defined");
			}
		}
		return elInput;
	}

	selectDisplay(arrSelectOptions, elParent)
	{
		if(array_key_exists("", arrSelectOptions))
			this.recursiveSelectDisplay(arrSelectOptions[""], elParent);

		for (let i in arrSelectOptions)
		{
			if(i !== "")
				this.recursiveSelectDisplay(arrSelectOptions[i], elParent);
		}
	}


	recursiveSelectDisplay(arrItemOptions, elParent)
	{
		if(array_key_exists("type", arrItemOptions) && arrItemOptions["type"] === "optgroup")
		{
			let elOptGroup = createElement("optgroup", {
				"label" : arrItemOptions["text"],
			});

			for(let i in arrItemOptions["data"])
			{
				this.recursiveSelectDisplay(arrItemOptions["data"][i], elOptGroup);
			}
			elParent.appendChild(elOptGroup);
		}
		else
		{
			let elOption = createElement(
				"option",
				{},
				arrItemOptions["text"]
			);
			elOption.value = arrItemOptions["value"];

			if(array_key_exists("disabled", arrItemOptions))
			{
				elOption.disabled = true;
			}
			elParent.appendChild(elOption);
		}
	}

	loadState(arrState)
	{
		if(arrState.bEditMode === true && this.bEditMode === false)
			this.arrSavedState = this.getState();

		this.bEditMode = arrState.bEditMode;

		let arrData = arrState.arrData;
		for(let strKey in arrData)
		{
			if(array_key_exists(strKey, this.arrFormInputs))
			{
				//this.arrFormInputs[strKey].value = arrData[strKey];
				//hack for jQuery plugin - select2
				$(this.arrFormInputs[strKey]).val(arrData[strKey]).trigger('change');

                const event = new Event('shadowjs:changed');
                this.arrFormInputs[strKey].dispatchEvent(event);
			}

			if(array_key_exists(strKey, this.arrFormCustomElements))
			{
				this.arrFormCustomElements[strKey].updateValue(arrData[strKey]);
			}
		}

		if(arrState.bEditMode)
		{
			if(this.elKeyDiv)
				this.elKeyDiv.style.display = "block";
			this.elButtonAdd.style.display = "none";
			this.elButtonSave.style.display = "inline-block";
			this.elButtonClose.innerText = "Close edit";
		}
		else
		{
			if(this.elKeyDiv)
				this.elKeyDiv.style.display = "none";
			this.elButtonAdd.style.display = "inline-block";
			this.elButtonSave.style.display = "none";
			this.elButtonClose.innerText = "Reset";
		}

		if(!this.bAllowAdd)
			if(this.bEditMode)
				this.elForm.style.display = "block";
			else
				this.elForm.style.display = "none";

		this.elButtonSave.firstElementChild.classList.add("hidden");
		this.elButtonAdd.firstElementChild.classList.add("hidden");


		for(let i in this.arrPlugins)
			this.arrPlugins[i].onLoadState(arrState);
	}

	resetState()
	{
		if(this.arrSavedState !== undefined)
			this.loadState(this.arrSavedState);
		else
		{
			for (let strKey in this.arrFormInputs)
			{
				if(this.arrFormInputs[strKey].tagName === 'SELECT')
					this.arrFormInputs[strKey].selectedIndex = 0;
				else
					this.arrFormInputs[strKey].value = "";

                const event = new Event('shadowjs:changed');
                this.arrFormInputs[strKey].dispatchEvent(event);
			}

			for (let strKey in this.arrFormCustomElements)
			{
				this.arrFormCustomElements[strKey].updateValue("");
			}
		}

		if(this.elKeyDiv)
			this.elKeyDiv.style.display = "none";

		this.elButtonAdd.style.display = "inline-block";
		this.elButtonSave.style.display = "none";

		this.elButtonSave.disabled = false;
		this.elButtonAdd.disabled = false;
		this.elButtonSave.firstElementChild.classList.add("hidden");
		this.elButtonAdd.firstElementChild.classList.add("hidden");
		this.elButtonSave.childNodes[1].nodeValue = "Save";

		this.bEditMode = false;
		for(let i in this.arrPlugins)
			this.arrPlugins[i].onResetState();
	}

	getState(bFinalState)
	{
		let arrObject = {};

		for(let strKey in this.arrFormInputs)
		{
			if(this.arrData["cols"][strKey]["ghost"] === undefined)
				arrObject[strKey] = this.arrFormInputs[strKey].value;
		}

		for(let strKey in this.arrFormCustomElements)
		{
			if(this.arrData["cols"][strKey]["ghost"] === undefined)
			{
				if(bFinalState)
					this.arrPromisesSubmit.push(this.arrFormCustomElements[strKey].prepareSubmit());

				arrObject[strKey] = this.arrFormCustomElements[strKey].getValue();
			}
		}

		for(let strKey in this.arrHiddenKeysValues)
		{
			arrObject[strKey] = this.arrHiddenKeysValues[strKey];
		}

		return {arrData: arrObject, bEditMode: this.bEditMode};
	}

	filterData(arrState)
	{
		for(let i in arrState)
		{
			if(arrState[i] === null)
				delete arrState[i];
		}
		return arrState;
	}

	addValidationFn(fnValidation)
	{
		this.arrValidationFns.push(fnValidation);
	}

	addPlugin(plugin)
	{
		this.arrPlugins.push(plugin);
	}

	addFormRow(strDisplayName, elContent)
	{
		const elDiv = document.createElement("div");
		elDiv.className = "form-group";
		const elLabelDiv = createElement("div", {class: "col-sm-2"});
		const elLabel = createElement("label", {class:"control-label", style:"width:100%"});
		elLabel.appendChild(document.createTextNode(strDisplayName));
		elLabelDiv.appendChild(elLabel);
		elDiv.appendChild(elLabelDiv);

		let elInputDiv = createElement("div", {class: "col-sm-10"});
		elInputDiv.appendChild(elContent);
		elDiv.appendChild(elInputDiv);
		this.elFormContent.appendChild(elDiv);
	}
}