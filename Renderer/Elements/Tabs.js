class Tabs extends BaseElement{

	constructor(elParent, arrData, arrOptions, strDefaultTabKey)
	{
		super(elParent, arrData, arrOptions);
		this.arrTabs = {};
		this.arrContents = {};
		this.arrState = {};
		this.strDefaultTabKey = strDefaultTabKey;
		this.bDisabled = false;
		this.strActiveTab = undefined;
	}

	render()
	{
		let elUL = document.createElement("ul");
		elUL.className = "nav nav-tabs";
		this.elUL = elUL;
		if(this.strUlClass)
			elUL.className += ' ' + this.strUlClass;


		let elDIV = document.createElement("div");
		elDIV.className="tab-content";
		this.elTabContent = elDIV;

		this.arrData.forEach((arrItem) => {
			this.addItem(arrItem, elUL);
		}, this);


		this.elParent.appendChild(elUL);
		this.elParent.appendChild(elDIV);

		if(this.strDefaultTabKey === undefined)
			$(this.arrTabs[this.arrData[0]["key"]]).children("a").tab('show');
		else
			$(this.arrTabs[this.strDefaultTabKey]).children("a").tab('show');
	}

	removeItem()
	{

	}

	addItem(arrItem, elParent)
	{
		let elLI = document.createElement("li");
		let elA = document.createElement("a");
		elA.href= "#"+arrItem["key"];
		elA.appendChild(document.createTextNode(arrItem["text"]));
		elA.addEventListener("click", (event) => {
			if(this.arrState[arrItem["key"]]["disabled"])
			{
				event.preventDefault();
				return false;
			}

			$(event.target).tab('show');
		});
		$(elA).on('shown.bs.tab', () => {
			this.strActiveTab = arrItem["key"];
			arrItem["activeCallback"](this.arrContents[arrItem["key"]]);
		});

		$(elA).on('hidden.bs.tab', () => {
			arrItem["hideCallback"](this.arrContents[arrItem["key"]]);
		});

		elLI.appendChild(elA);
		this.arrTabs[arrItem["key"]] = elLI;
		elParent.appendChild(elLI);

		let elDIV = document.createElement("div");
		elDIV.className = "tab-pane fade";
		elDIV.id = arrItem["key"];
		this.elTabContent.appendChild(elDIV);

		this.arrContents[arrItem["key"]] = elDIV;

		this.arrState[arrItem["key"]] = {};
		this.arrState[arrItem["key"]]["disabled"] = false;
	}

	disableOthers()
	{
		for(let i in this.arrTabs)
		{
			if(i !== this.strActiveTab)
			{
				this.arrTabs[i].classList.add("disabled");
				this.arrState[i]["disabled"] = true;
			}
		}
	}

	enableAll()
	{
		for(let i in this.arrTabs)
		{
			this.arrTabs[i].classList.remove("disabled");
			this.arrState[i]["disabled"] = false;
		}
	}

	disableOption(strKey)
	{
		this.arrTabs[strKey].classList.add("disabled");
		this.arrState[strKey]["disabled"] = true;
	}

	enableOption(strKey)
	{
		this.arrTabs[strKey].classList.remove("disabled");
		this.arrState[strKey]["disabled"] = false;
	}

	setUlClass(strClass)
	{
		this.strUlClass = strClass;
	}
}