class BaseElement{

	static get classes()
	{
		return {
			Multiselect,
			ColorRow,
			Image,
			MultiImage,
			File,
			BootstrapWYSIWYG
		}
	};

	/**
	 *
	 * @param {Element} elParent
	 * @param arrData
	 * @param arrSelectValues
	 */
	constructor(elParent, arrData, arrSelectValues)
	{
		this.elParent = elParent;
		this.arrData = arrData;
		this.arrOptions = arrSelectValues;
	}

	render()
	{

	}

	removeItem()
	{

	}

	addItem()
	{

	}
}