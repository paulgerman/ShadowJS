class TreeView extends BaseElement{
	render()
	{
		let elDIV = document.createElement("div");
		elDIV.setAttribute("style", "min-height: 500px; width: 100%;");

		let elTable = document.createElement("table");
		elTable.className = "tree table table-responsive";

		let elH1 = document.createElement("h1");
		elH1.appendChild(document.createTextNode("Basic tree view:"));
		this.elParent.appendChild(elH1);

		let elTBody = document.createElement("tbody");

		this.arrData.forEach((arrItem) => {
			this.addItem(arrItem, elTBody);
		}, this);
		elTable.appendChild(elTBody);
		elDIV.appendChild(elTable);
		this.elParent.appendChild(elDIV);
		$(elTable).treegrid();
	}

	removeItem()
	{

	}

	addItem(arrItem, elParent)
	{
		let elTR = document.createElement("tr");
		elTR.classList.add("treegrid-" + arrItem["id"]);
		if(arrItem["parent"] !== undefined)
			elTR.classList.add("treegrid-parent-" + arrItem["parent"]);

		let elTD = document.createElement("td");
		elTD.appendChild(document.createTextNode(arrItem["text"]));
		elTR.appendChild(elTD);

		elParent.appendChild(elTR);
	}
}