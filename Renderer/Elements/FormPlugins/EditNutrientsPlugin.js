class EditNutrientsPlugin extends BaseFormPlugin{

	//arrOptions = {nutrients: [...]"}


	initialize()
	{
		this.arrNutrientsValues = {};
		this.arrItemNutrients = {};

		const arrNutrients = this.arrOptions["nutrients"];

		const elDiv = createElement("div");

		const elButtons = createElement("div");

		this.elRadioNo = createElement("input", {type: "radio", name:"optradio", checked: true});
		this.elRadioYes = createElement("input", {type: "radio", name:"optradio"});

		this.elRadioNo.addEventListener("click", (e) => {
			const bConfirm = confirm('Are you sure?');
			if(bConfirm !== true)
				this.elRadioYes.checked = true;
		});

		this.elRadioNo.addEventListener("change", this.changeMenu.bind(this));
		this.elRadioYes.addEventListener("change", this.changeMenu.bind(this));


		elButtons.appendChild(createElement("label", {class:"radio-inline"}, [this.elRadioNo, createText("No")]));
		elButtons.appendChild(createElement("label", {class:"radio-inline"}, [this.elRadioYes, createText("Yes")]));

		this.form.addFormRow("Nutrients?", elButtons);

		this.elContent = createElement("div");

		let nCount = 0;

		let elRow;
		for(let i in arrNutrients)
		{
			if(nCount % 2 === 0)
				elRow = createElement("div", {class:"row form-inline"});


			const elCol = createElement("div", {class:"col-xs-6"});
			if(nCount % 2 === 0)
				elCol.style = "border-right: 1px solid black; padding-bottom: 2px;";

			const elNutrientName = createElement("div", {class:"col-xs-3 text-right", style:"height: 31px; line-height:15px; display: flex; justify-content: center; flex-direction: column;"}, arrNutrients[i]["nutrient_name"]);
			const elQuantityInput = createElement("input", {step: "0.00001", class:"form-control no-spinners", /*required: true,*/ type:"number", style:"width:70px;"});
			const elGramsInput = createElement("input", {step: "0.00001", class:"form-control no-spinners", /*required: true,*/ type:"number", style: "width:70px"});
			const elUnitSelect = createElement("select", {class:"form-control", style: "width:82px"});
			const arrUnitMeasures = ['gms', 'mgs', 'mcgs'];
			for(let j in arrUnitMeasures)
                elUnitSelect.appendChild(createElement("option", {value: arrUnitMeasures[j]}, arrUnitMeasures[j]));

			/*const fnUpdateQuantity =  () => {
				elGramsInput.required = elQuantityInput.value === "";
			};
			const fnUpdateGrams = () => {
				elQuantityInput.required = elGramsInput.value === "";
			};
			elQuantityInput.addEventListener("keyup", fnUpdateQuantity);
			elGramsInput.addEventListener("keyup", fnUpdateGrams);*/


			const elColRow = createElement("div", {class: "row"});
			elColRow.appendChild(elNutrientName);
			elColRow.appendChild(createElement("div", {class:"col-sm-3"}, [elQuantityInput, createText("%")]));
			elColRow.appendChild(createElement("div", {class:"col-sm-3"}, [elGramsInput, createText(" in ")]));
			elColRow.appendChild(createElement("div", {class:"col-sm-3"}, [elUnitSelect]));
			elCol.appendChild(elColRow);

			elRow.appendChild(elCol);
			this.arrNutrientsValues[arrNutrients[i]["nutrient_id"]] = {
				quantity: elQuantityInput,
				grams: elGramsInput,
				unit: elUnitSelect
			};
			this.elContent.appendChild(elRow);
			nCount++;
		}


		this.changeMenu();
		this.updateInputs();

		elDiv.appendChild(this.elContent);
		this.form.elFormContent.appendChild(elDiv);
		console.log(elDiv);
	}



	onLoadState(arrState)
	{
		console.log("loading nutrients for ingredient " + arrState.arrData["item_id"]);
		call("itemNutrient_get_all", [0,99999,{
			"col": "item_id", "op": "=", "val": arrState.arrData["item_id"]
		}]).then((arrItems) => {
			this.arrItemNutrients = arrItems;
			this.updateInputs();
		});
	}

	onResetState()
	{
		console.log("state reset");
		this.arrItemNutrients = {};
		this.updateInputs();
	}

	afterSubmit(arrData)
	{
		this.arrItemNutrients = {};
		call("itemNutrient_delete_all", [" WHERE `item_id` = " + arrData["item_id"]]).then(() => {
			if(this.elRadioNo.checked)
				return;


			const arrRequests = [];

			for(let i in this.arrNutrientsValues)
			{
				if(this.arrNutrientsValues[i].quantity.value == 0 && this.arrNutrientsValues[i].grams.value == 0)
					continue;

				arrRequests.push({
					method: "itemNutrient_create",
					params: [{
						item_id: arrData["item_id"],
						nutrient_id: i,
						item_nutrient_quantity: this.arrNutrientsValues[i].quantity.value,
						item_nutrient_grams: this.arrNutrientsValues[i].grams.value,
						item_nutrient_unit: this.arrNutrientsValues[i].unit.value,
					}]
				});
			}

			if(arrRequests.length !== 0)
				batchCall(arrRequests, (results) => {
					for(let i in results)
						if(results[i].error !== undefined)
							alert("ERROR WHEN SAVING NUTRIENTS!!!");
				}, () => {alert("ERROR WHEN SAVING NUTRIENTS!!!")});
		});
	}

	changeMenu()
	{
		if(this.elRadioNo.checked)
		{
			this.elContent.style.display = "none";
		}
		if(this.elRadioYes.checked)
		{
			this.elContent.style.display = "block";
		}

	}

	updateInputs()
	{
		console.log("this item has " + Object.keys(this.arrItemNutrients).length + "nutrients");
		for(let i in this.arrNutrientsValues)
		{
			this.arrNutrientsValues[i].quantity.value = "";
			this.arrNutrientsValues[i].grams.value = "";
			this.arrNutrientsValues[i].unit.value = "gms";
		}

		for(let i in this.arrItemNutrients)
		{
			const nNutrientID = this.arrItemNutrients[i]["nutrient_id"];
			this.arrNutrientsValues[nNutrientID].quantity.value = this.arrItemNutrients[i]["item_nutrient_quantity"];
			this.arrNutrientsValues[nNutrientID].grams.value = this.arrItemNutrients[i]["item_nutrient_grams"];
			this.arrNutrientsValues[nNutrientID].unit.value = this.arrItemNutrients[i]["item_nutrient_unit"];

			/*this.arrNutrientsValues[nNutrientID].grams.required = this.arrItemNutrients[i]["item_nutrient_quantity"] == "";
			this.arrNutrientsValues[nNutrientID].quantity.required = this.arrItemNutrients[i]["item_nutrient_grams"] == "";*/
		}

		if(Object.keys(this.arrItemNutrients).length === 0)
			this.elRadioNo.checked = true;
		else
			this.elRadioYes.checked = true;
		this.changeMenu();
	}
}