class BaseFormPlugin{
	/**
	 * @param {Form} form
	 * @param {Object} arrOptions
	 */
	constructor(form, arrOptions)
	{
		this.form = form;
		this.arrOptions = arrOptions;
	}

	initialize()
	{
		console.error("Form Plugin Initialize not defined");
	}

	onLoadState(arrState)
	{
		console.error("Not defined");
	}

	onResetState()
	{
		console.error("Not defined");
	}

	beforeSubmit(arrData)
	{
		console.error("Not defined");
	}

	afterSubmit(arrData)
	{
		console.error("Not defined");
	}
}