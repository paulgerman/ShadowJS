class PhoneNumberPlugin extends BaseFormPlugin{

	//arrOptions = {key: "FORM_KEY"}

	initialize()
	{
		const strKey = this.arrOptions["key"];
		const elInputPhone = this.form.arrFormInputs[strKey];
		elInputPhone.addEventListener('keyup', (e) => {
			e.target.value = this.phoneFormat(e.target.value);
		});
		elInputPhone.addEventListener('focus', (e) => {
			e.target.value = this.phoneFormat(e.target.value);
		});

		const elLabel = createElement("label", {for: "checkbox" + strKey}, UNKNOWN + "? ");
		const elCheckboxUnknown = createElement("input", {id: "checkbox" + strKey, type: "checkbox", style: "width:13px; height: 13px;"});
		elCheckboxUnknown.addEventListener("change", (e) => {
			if(e.target.checked) {
				elInputPhone.disabled = true;
				elInputPhone.value = UNKNOWN;
			} else {
				elInputPhone.disabled = false;
				elInputPhone.value = "";
			}
		});
		elInputPhone.parentNode.insertBefore(elLabel, elInputPhone);
		elInputPhone.parentNode.insertBefore(elCheckboxUnknown, elInputPhone);

		this.elInputPhone = elInputPhone;
		this.elCheckboxUnknown = elCheckboxUnknown;
	}

	onLoadState(arrState)
	{
		if(this.elInputPhone.value !== UNKNOWN)
		{
			this.elInputPhone.disabled = false;
			this.elCheckboxUnknown.checked = false;
		}
		else
		{
			this.elInputPhone.disabled = true;
			this.elCheckboxUnknown.checked = true;
		}
	}

	onResetState()
	{

	}

	afterSubmit(arrData)
	{
	}

	phoneFormat(input){
		// Strip all characters from the input except digits
		input = input.replace(/\D/g,'');

		// Trim the remaining input to ten characters, to preserve phone number format
		input = input.substring(0,10);

		// Based upon the length of the string, we add formatting as necessary
		let size = input.length;
		if(size < 5){
			input = input;
		}else if(size < 8){
			input = input.substring(0,4)+' '+input.substring(4,7);
		}else{
			input = input.substring(0,4)+' '+input.substring(4,7)+' '+input.substring(7,10);
		}
		return input;
	}
}