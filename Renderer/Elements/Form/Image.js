class Image extends BaseFormElement{
	/**
	 * @param {Element} elParent
	 * @param arrData
	 * @param arrOptions
	 * @param {Form} form
	 * @param {String|undefined} strKey
	 */
	constructor(elParent, arrData, arrOptions, form, strKey)
	{
		super(elParent, arrData, arrOptions, form, strKey);
		this.strFile = "";
	}

	render()
	{
		if(this.arrData !== "")
		{
			let elDiv = document.createElement("div");
			elDiv.setAttribute("style", "background-color: grey; background-size: 20%; display: inline-block;");

			let elImg = document.createElement("img");
			elImg.src = this.arrData;
			elImg.setAttribute("style", "max-width:50px");

			elDiv.appendChild(elImg);

			this.elParent.appendChild(elDiv);
		}
		else
		{
			this.elParent.appendChild(document.createTextNode("no image"));
		}
	}

	renderEdit()
	{
		let elDiv = document.createElement("div");
		elDiv.addEventListener("click", (e) => {
			if(e.target === elDiv || e.target === elImg)
				$(this.elInput).trigger('click');
		});
		elDiv.setAttribute("style", "position:relative; background-color: grey; background-size: 20%; display: inline-block; vertical-align: middle;");

		let elImg = document.createElement("img");
		elImg.setAttribute("style", "max-width:160px; cursor:pointer;");
		elImg.src = "img/add_image.png";
		elImg.alt = "Image preview";

		this.elImg = elImg;

		elDiv.appendChild(elImg);

		let elButton = createElement(
			"button",
			{
				class : "btn-danger btn-sm",
				style : "display: none; vertical-align: middle;",
				type : "button"
			},
			"x"
		);
		elButton.addEventListener("click", () => {
			this.nCropCount = 0;
			this.strFile = "";
			this.elImg.src = "img/add_image.png";
			this.elInput.value = "";
			elButton.style.display = "none";
			if (this.arrOptions["internalAttributes"] !== undefined && this.arrOptions["internalAttributes"]["required"] !== undefined)
				this.elInput.required = true;

			if(this.arrOptions["cropRatio"])
				$(elImg).cropper('destroy');

			this.elInput.style.display = "block";

		});

		this.elButton = elButton;

		let elInput = createElement(
			"input",
			{
				type : "file",
				style: "position:absolute; top:0; width:100%; height:100%; opacity:0; cursor: pointer;"
			}
		);
		this.elInput = elInput;

		elInput.addEventListener("change", () => {
			if(elInput.files[0])
			{
				let nFileSize = elInput.files[0].size;
				let maxSize;

                if(typeof nMaxImageSize === 'undefined')
                    maxSize = nMaxFileSize;
                else
                    maxSize = nMaxImageSize;

				if(nFileSize >= maxSize)
				{
					elInput.value = "";
					alert("Maximum size of image is " + (maxSize/1024.0/1024.0).toFixed(2) + "MB. Your image has " + (nFileSize/1024.0/1024.0).toFixed(2) + "MB");
					return;
				}

				let regex = /^([a-zA-Z0-9\s_\\.\-:()&])+(.jpg|.jpeg|.gif|.png)$/;
				if (!regex.test(elInput.files[0].name.toLowerCase()))
				{
					alert(elInput.files[0].name + " is not a valid image name!");
					elInput.value = "";
					return;
				}
				this.strFile = {
					element: $(this.elInput).clone(),
					text: elInput.files[0].name
				};

				elImg.src = URL.createObjectURL(elInput.files[0]);
				this.elButton.style.display = "inline-block";

				this.startCropper();
			}
		});

		elDiv.appendChild(elInput);
		this.elParent.appendChild(elDiv);
		this.elParent.appendChild(elButton);
	}

	updateValue(arrData)
	{
		this.nCropCount = 0;
		this.strFile = arrData;

		if (typeof arrData === 'string')
		{
			if(this.arrOptions["cropRatio"])
				$(this.elImg).cropper('destroy');

			this.elInput.value = "";
			if(arrData !== '')
			{
				this.elImg.src = arrData;
				this.startCropper();
			}
			else
				this.elImg.src = 'img/add_image.png';
		}
		else
		{
			$(this.elInput).replaceWith(arrData.element);
			this.elInput = arrData.element.get(0);
			this.elImg.src = URL.createObjectURL(this.elInput.files[0]);
			this.startCropper();
		}


		if(this.strFile !== "")
		{
			this.elButton.style.display = "inline-block";
		}
		else
		{
			this.elButton.style.display = "none";
		}

		if(
			this.arrOptions["internalAttributes"] !== undefined
			&& this.arrOptions["internalAttributes"]["required"] !== undefined
			&& (
				!this.form.bEditMode
				|| this.strFile === ""
			)
		)
			this.elInput.required = true;
		else
			this.elInput.required = false;
	}

	getValue()
	{
		return this.strFile;
	}

	getTextValue()
	{
		return "";
	}

	getPreviewValue()
	{
		return this.getTextValue();
	}

	async getBlob(strFileExt)
	{
		if(strFileExt.toLowerCase() === "png")
			return new Promise((resolve, reject) => {
				$(this.elImg).cropper('getCroppedCanvas').toBlob((blob) => {
						resolve(blob);
					}, 'image/png');
			});
		else
			return new Promise((resolve, reject) => {
				$(this.elImg).cropper('getCroppedCanvas').toBlob((blob) => {
					resolve(blob);
				}, 'image/jpeg', 0.92);
			});
	}

	async prepareSubmit()
	{
		if(this.elInput.files.length || this.nCropCount > 1)
		{
			let formData = new FormData();

			if(this.arrOptions["cropRatio"])
			{
				let strFileName;
				if(this.elInput.files.length)
					strFileName = this.elInput.files[0].name;
				else
				{
					strFileName = this.elImg.src;
					console.log(this.nCropCount);
				}

				const strFileExt = strFileName.substring(strFileName.lastIndexOf('.') + 1, strFileName.length) || strFileName;
				formData.append('image', await this.getBlob(strFileExt), strFileName);
			}
			else
				formData.append('image', this.elInput.files[0], this.elInput.files[0].name);


			const query = '{"method":"image_upload","params":[' + (this.arrOptions["maxWidthOrHeight"]?this.arrOptions["maxWidthOrHeight"]:"") + ']}';
			formData.append('q', query);

			return new Promise((resolve, reject) => {
				$.ajax({
					url:  $.jsonRPC.endPoint,
					type: "POST",
					data: formData,
					processData: false,  // tell jQuery not to process the data
					contentType: false   // tell jQuery not to set contentType
				}).then((answer) =>
				{
					if(answer.result === undefined)
						console.log(answer);
					else
						console.log("Uploaded image (size "+this.arrOptions["maxWidthOrHeight"]+") "+this.strKey+": "+answer.result);
					resolve ({
						"key": this.strKey,
						"value": answer.result
					});
				}, (error) =>
				{
					console.error(error);
					reject(error);
				});
			});
		}
	}

	startCropper()
	{
		if(this.arrOptions["cropRatio"])
		{
			this.elInput.style.display = "none";

			$(this.elImg).cropper({
				aspectRatio: this.arrOptions["cropRatio"],
				viewMode: 2,
				autoCropArea: 1,
				crop: (e) => {
					this.nCropCount++;
				}
			});
		}
	}

	static isRenderText()
	{
		return false;
	}

	static isRenderHTML()
	{
		return true;
	}
}