class Multiselect extends BaseFormElement{
	render()
	{
		let arrSelectableItems = {};
		for(let i in this.arrOptions)
			$.extend(arrSelectableItems, Renderer._getSelectableItems(this.arrOptions[i]));

		if(this.arrData.length > 0)
		{
			if(arrSelectableItems[this.arrData[0]] === undefined)
				console.error("Select options have not been defined. DID YOU ADD THE arrSelectOptions to the table constructor?");
			this.elParent.appendChild(document.createTextNode(arrSelectableItems[this.arrData[0]]["text"]));
		}
		for(let i = 1; i < this.arrData.length; i++)
		{
			this.elParent.appendChild(document.createTextNode(", " + arrSelectableItems[this.arrData[i]]["text"]));
		}
	}

	renderEdit()
	{
		this.elInput = this.form.defaultElementEdit(this.strKey, this.elParent);
		let arrInternalTypeOptions = {
			query: function(options) {
				var selectedIds = options.element.select2('val');
				var selectableGroups = $.map(this.data, function(group) {
					var areChildrenAllSelected = true;
					$.each(group.children, function(i, child) {
						if (selectedIds.indexOf(child.id) < 0) {
							areChildrenAllSelected = false;
							return false; // Short-circuit $.each()
						}
					});
					return !areChildrenAllSelected ? group : null;
				});
				options.callback({ results: selectableGroups });
			}
		};
		if(array_key_exists("internalTypeOptions", this.arrOptions))
			arrInternalTypeOptions = this.arrOptions["internalTypeOptions"];

		this.select2 = $(this.elInput).select2(arrInternalTypeOptions).on('select2-selecting', function(e) {
			var $select = $(this);
			if (e.val == '') {
				e.preventDefault();
				$select.select2('data', $select.select2('data').concat(e.choice.children));
				$select.select2('close');
			}
		});
	}

	updateValue(arrData)
	{
		if(this.fnCallback !== undefined)
			this.fnCallback(arrData);

		this.select2.val(arrData).trigger("change");
	}

	//used in ROB project
	setCallBackBeforeUpdateValue(fnCallback)
	{
		this.fnCallback = fnCallback;
	}

	getValue()
	{
		return this.select2.val();
	}

	getTextValue()
	{
		let arrSelectableItems = {};
		let strValue = "";
		arrSelectableItems = Renderer._getSelectableItems(this.arrOptions);

		if(this.arrData.length > 0)
			if(arrSelectableItems[this.arrData[0]] !== undefined)
				strValue += arrSelectableItems[this.arrData[0]]["text"];
			else
				strValue += this.arrData[0];

		for(let i = 1; i < this.arrData.length; i++)
		{
			if(arrSelectableItems[this.arrData[i]] !== undefined)
				strValue += ", " + arrSelectableItems[this.arrData[i]]["text"];
			else
				strValue += ", " + this.arrData[i];
		}

		return strValue;
	}

	getPreviewValue()
	{
		return this.getTextValue();
	}


	static isRenderText()
	{
		return true;
	}

	static isRenderHTML()
	{
		return false;
	}
}