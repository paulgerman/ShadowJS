class ColorRow extends BaseFormElement{
	render()
	{
		let elParentParent = this.elParent.parentNode;
		if(elParentParent !== null && elParentParent.tagName === 'TR')
		{
			let color;
			switch(this.arrData)
			{
				case "red":
					color = '#FF9999';
					break;
				case "yellow":
					color = '#ffffb3';
					break;
				case "orange":
					color = '#ffcc99';
					break;
				case "green":
					color = '#80ffbf';
					break;
			}

			elParentParent.style["background-color"] = color;
		}

		this.elParent.appendChild(document.createTextNode(this.arrData));
	}

	renderEdit()
	{
		this.elInput = this.form.defaultElementEdit(this.strKey, this.elParent);
	}

	updateValue(arrData)
	{
		if(arrData === "")
			this.elInput.selectedIndex = 0;
		else
			this.elInput.value = arrData;
	}

	getValue()
	{
		return this.elInput.value;
	}

	getTextValue()
	{
		return this.arrData;
	}

	getPreviewValue()
	{
		return this.getTextValue();
	}

	static isRenderText()
	{
		return false;
	}

	static isRenderHTML()
	{
		return true;
	}
}