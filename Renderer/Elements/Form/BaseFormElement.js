class BaseFormElement{
	/**
	 * @param {Element} elParent
	 * @param arrData
	 * @param arrOptions
	 * @param {Form} form
	 * @param {String|undefined} strKey
	 */
	constructor(elParent, arrData, arrOptions, form, strKey)
	{
		this.form = form;
		this.strKey = strKey;
		this.elParent = elParent;
		this.arrData = arrData;
		this.arrOptions = arrOptions;
	}

	render()
	{
		console.error("Render not defined");
	}

	renderEdit()
	{
		console.error("Render edit not defined");
	}

	updateValue(arrData)
	{
		console.error("Update value not defined");
	}

	getValue()
	{
		console.error("Get value not defined");
	}

	getTextValue()
	{
		console.error("Get text value not defined");
	}

	getPreviewValue()
	{
		return this.getTextValue();
	}

	prepareSubmit()
	{
		return;
	}

	static isRenderText()
	{
		return false;
	}

	static isRenderHTML()
	{
		return false;
	}
}