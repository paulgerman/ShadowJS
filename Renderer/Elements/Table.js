class Table extends BaseElement{
	constructor(elParent, arrData, arrOptions, arrActions, arrHiddenKeys, arrSelectValues, strTableDOMID, strClassName)
	{
		super(elParent, arrData, arrOptions);
		this.arrActions = Array.isArray(arrActions)? arrActions : [];
		this.arrHiddenKeys = Array.isArray(arrHiddenKeys) ? arrHiddenKeys : [];
		this.arrSelectValues = (typeof arrSelectValues === "object")? arrSelectValues : {};
		this.strTableDOMID = strTableDOMID;
		this.strClassName = strClassName;
		this.arrColumns = [];
		this.arrPlugins = [];
		this.dataTable = null;
	}


	render()
	{
		let elTable = document.createElement("table");
		this.elTable = elTable;
		if(this.strTableDOMID !== undefined)
			this.elTable.id = this.strTableDOMID;
		elTable.className = "table table-hover table-striped " + (this.strClassName===undefined?"":this.strClassName);

		let columnDefs = [];
		for(let strKey in this.arrOptions["cols"])
		{
			let arrOptions = this.arrOptions["cols"][strKey];
			if(!isString(arrOptions) && this.arrHiddenKeys.indexOf(strKey) === -1)
			{
				let arrCol = {
					title: arrOptions["displayName"],
					data: strKey,
					className: strKey,
					name: strKey
				};
				this.arrColumns.push(arrCol);


				if(arrOptions["internalType"] !== undefined)
				{
					if(BaseElement.classes[arrOptions["internalType"]] !== undefined)
					{
						let classBase = BaseElement.classes[arrOptions["internalType"]];
						arrOptions["internalType"] = capitalizeFirstLetter(arrOptions["internalType"]);

						if(classBase.isRenderText())
						{
							let strKeyTmp = strKey;
							arrCol["render"] = (data, type, full, meta) =>
							{
								//let api = new $.fn.dataTable.Api(meta["settings"]);
								let baseElement = new BaseElement.classes[arrOptions["internalType"]](undefined, full[strKeyTmp], this.arrSelectValues[strKeyTmp]);
								return baseElement.getPreviewValue();
							};
						}

						if(classBase.isRenderHTML())
						{
							//WOW #priceless #moriprost #CHROMEvsOTHERS
							let strKeyTmp = strKey;
							columnDefs.push({
								targets: this.getColumnIndexesWithClass(this.arrColumns, strKey),
								createdCell: (td, cellData, rowData, row, col) =>
								{
									if(td.firstChild)
										td.removeChild(td.firstChild);
									let baseElement = new BaseElement.classes[arrOptions["internalType"]](td, cellData, this.arrSelectValues[strKeyTmp], this);
									baseElement.render();
								}
							});
						}
					}
					else
					{
						arrCol["render"] = (data, type, full, meta) => {
							return 'not yet available';
						};
					}
				}
				else
				{
					columnDefs.push({
						targets: this.getColumnIndexesWithClass( this.arrColumns, strKey ),
						createdCell: (td, cellData, rowData, row, col) => {
							if(arrOptions["maxDisplaySize"] !== undefined && cellData.length >= arrOptions["maxDisplaySize"])
							{
								td.firstChild.nodeValue = cellData.substring(0,arrOptions["maxDisplaySize"])+"...";
							}
							if(cellData !== null && cellData.length > 4 && cellData.substring(0,4) === "http")
							{
								td.replaceChild(createElement("a", {href: cellData, target:"_blank", style: "color: darkblue;"}, (cellData.length > 45?cellData.substring(0,45)+"...":cellData)),td.firstChild);
							}
						}
					});
				}
			}
		}

		if(this.arrActions.length)
		{
			this.arrColumns.push(
				{
					title: (this.strActionsText?this.strActionsText:"---"),
					data: null,
					className: "actions all",
					defaultContent: ""
				}
			);

			columnDefs.push({
				targets: this.getColumnIndexesWithClass(this.arrColumns, "actions all"),
				createdCell: (td, cellData, rowData, row, col) => {
					this.generateActions(td, rowData[this.arrOptions["indexKey"]]);
				}
			 });
		}
		this.elParent.appendChild(elTable);

		let arrOption = {
			stateSave: true,
			stateDuration: 60 * 60 * 24 * 31,
			dom: "Bfrltip",
			buttons: [
				'colvis', 'excel'
			],
			colReorder: true,
			fixedHeader: true,
			columns: this.arrColumns,
			data: [],
			columnDefs : columnDefs,
			autoWidth: false,
			deferRender: true
		};

		if(this.arrDataTableConfig)
		{
			$.extendext(true, 'concat', arrOption, this.arrDataTableConfig);
		}

		this.dataTable = $(elTable).DataTable(arrOption).search("").draw();

		//Search ignoring diacritics
		jQuery.fn.DataTable.ext.type.search.string = function ( data ) {
			return !data ?
				'' :
				typeof data === 'string' ?
					data
						.replace(/\n/g, ' ')
						.replace(/[ãáÁăĂâÂ]/g, 'a')
						.replace(/[éÉêè]/g, 'e')
						.replace(/[üÜ]/g, 'u')
						.replace(/[íÍîÎ]/g, 'i')
						.replace(/[õóÓô]/g, 'o')
						.replace(/[țȚ]/g, 't')
						.replace(/[șȘ]/g, 's'):
					data;
		};

		$('.dataTables_filter input').unbind().keyup( (e, settings) => {
			this.dataTable
				.search(
					jQuery.fn.DataTable.ext.type.search.string( e.target.value )
				)
				.draw();
		});

		this.intervalUpdateFixedHeader = setInterval(() => {
			if( ! $.fn.DataTable.isDataTable( this.dataTable.table().node() ))
				clearInterval(this.intervalUpdateFixedHeader);
			this.dataTable.fixedHeader.adjust();
		},1500);

		for(let i in this.arrData)
			this.addItem(this.arrData[i]);

		this.dataTable.draw();


		//hackish because of datatable bug that re-renders cells when column reorder without calling createdCell()
		this.dataTable.on('column-reorder', (e, settings, details) =>  {
			this.dataTable.clear();
			for(let i in this.arrData)
			{
				this.addItem(this.arrData[i]);
			}
			this.dataTable.draw(false);
		});

		for(const i in this.arrPlugins)
			this.arrPlugins[i].initialize();
	}

	removeItem(nID, bFade)
	{
		if(bFade === undefined)
			bFade = true;

		let elRow = document.getElementById(this.arrOptions["objectType"] + nID);
		if(bFade)
		{
			$(elRow).fadeOut(
				300,
				'swing',
				() =>
				{
					this.dataTable
						.row(elRow)
						.remove()
						.draw(false);
				}
			);
		}
		else
			this.dataTable
				.row(elRow)
				.remove()
				.draw(false);

		delete this.arrData[nID];
	}

	removeAll()
	{
		this.dataTable.clear().draw();
	}


	addItem(arrObject)
	{
		let arrData = {};
		arrData["DT_RowId"] = this.arrOptions["objectType"]+arrObject[this.arrOptions["indexKey"]];
		let arrEnumKeys = this.arrOptions["enumKeys"] === undefined ? [] : this.arrOptions["enumKeys"];
		for(let strKey in this.arrOptions["cols"])
		{
			if(!this.arrOptions["cols"].hasOwnProperty(strKey))
				continue;

			let arrOptions = this.arrOptions["cols"][strKey];
			if(isString(arrOptions) || this.arrHiddenKeys.indexOf(strKey) !== -1)
				continue;


			let nSizeLimit = 999999;
			if(arrOptions["maxDisplaySize"] !== undefined)
				nSizeLimit = arrOptions["maxDisplaySize"];

			if(arrOptions["displayOptions"] !== undefined)
			{
				let arrDisplayOptions = arrayCombine(arrEnumKeys[strKey], arrOptions["displayOptions"]);
				arrData[strKey] = arrDisplayOptions[arrObject[strKey]];
			}
			/*else if(Array.isArray(arrObject[strKey]) && arrOptions["internalType"] === undefined)
			{
				//arrData[strKey] = arrObject[strKey].join(", ");
				arrData[strKey] = arrObject[strKey];
			}*/

			else if (this.arrSelectValues[strKey] && this.arrOptions["cols"][strKey]["internalType"] === undefined)
			{
				if(Array.isArray(this.arrSelectValues[strKey]))
					throw new Error("arrSelectValues["+strKey+"] must be object, not array!");

				if(this.arrSelectValues[strKey][arrObject[strKey]])
				{
					arrData[strKey] = this.arrSelectValues[strKey][arrObject[strKey]]["text"];
				}
				else
				{
					console.error("Select value "+ arrObject[strKey] +" not found in arrSelectValues["+strKey+"]");
					arrData[strKey] = arrObject[strKey];
				}
			}
			else
			{
				arrData[strKey] = arrObject[strKey];
			}
		}

		this.dataTable.row.add(arrData);
		this.arrData[arrObject[this.arrOptions["indexKey"]]] = arrObject;
	}


	getColumnIndexesWithClass( columns, className ) {
		let indexes = [];
		$.each( columns, function( index, columnInfo ) {
			// note: doesn't attempt to support multiple class names on a column
			if( columnInfo.className == className ) {
				indexes.push( index );
			}
		} );

		return indexes;
	}

	generateActions(elParent, nKey)
	{
		if(this.arrActions.length)
		{
			let elTD = document.createElement("td");
			for(let i in this.arrActions)
			{
				if(this.arrActions[i] instanceof Function)
					this.arrActions[i](nKey, elTD);
				else
				{
					let arrAction = this.arrActions[i];
					let elButton = document.createElement("button");
					elButton.type = "button";
					elButton.className = "btn btn-primary btn-sm";
					if(arrAction["class"])
						elButton.className += ' ' + arrAction["class"];
					elButton.setAttribute("style", "margin-bottom:2px;");
					elButton.appendChild(document.createTextNode(arrAction["text"]));

					if (arrAction["callback"] !== undefined)
					{
						elButton.addEventListener('click', (event) =>
						{
							event.stopPropagation();
							event.target.disabled = true;
							arrAction["callback"](nKey, event.target);
						});
					}

					elTD.appendChild(elButton);
					elTD.appendChild(document.createElement("br"));
				}
			}
			elParent.appendChild(elTD);
		}
	}

	setTableDOMID(strTableDOMID)
	{
		this.strTableDOMID = strTableDOMID;
	}
	setActionsText(strActionsText)
	{
		this.strActionsText = strActionsText;
	}

	setDataTableConfig(arrConfig)
	{
		this.arrDataTableConfig = arrConfig;
	}

	addPlugin(plugin)
	{
		this.arrPlugins.push(plugin);
	}
}