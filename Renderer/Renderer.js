class Renderer {
	constructor(elParent, arrConfig, arrRenderConfigs) {
		this.elParent = elParent;
		this.arrConfig = arrConfig;
		this.arrRenderConfigs = arrRenderConfigs;

		if(!arrRenderConfigs || !arrRenderConfigs[this.constructor.name])
            throw "Config for " + this.constructor.name + " not loaded!";

        this.arrRenderConfig = arrRenderConfigs[this.constructor.name];
        this.arrToRender = [];
    }


	static _getSelectableItems(arrItemOptions)
	{
		let arrItems = {};

		for(let i in arrItemOptions)
		{
			if(array_key_exists("type", arrItemOptions[i]) && arrItemOptions[i]["type"] === "optgroup")
			{
				$.extend(arrItems, Renderer._getSelectableItems(arrItemOptions[i]["data"]));
			}
			else
				arrItems[arrItemOptions[i]["value"]] = arrItemOptions[i];
		}

		return arrItems;
	}

    renderAll()
    {
        for(let i = 0; i < this.arrToRender.length; i++)
	        this.arrToRender[i].render();

        this.arrToRender = [];
    }
}